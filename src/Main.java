/*
AUTHOR : ELLDA ARTHA AIRLANGGA
*/

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String pilihMenu, pilihBidang, keluar;

        double phi = 3.14D;

        Scanner keyboard = new Scanner(System.in);

        do {
            System.out.println("\n\n--------------------------------------");
            System.out.println("Kalkukaltor Penghitung Luas dan Volum");
            System.out.println("--------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volum");
            System.out.println("3. Tutup Aplikasi");
            System.out.println("--------------------------------------");
            System.out.print("Masukan Pilihan -> ");
            pilihMenu = keyboard.nextLine();
            switch (pilihMenu){
                case "1": {
                    System.out.println("----------------------------------");
                    System.out.println("Pilih bidang yang akan dihitung");
                    System.out.println("----------------------------------");
                    System.out.println("1. Persegi");
                    System.out.println("2. Lingkaran");
                    System.out.println("3. Segitiga");
                    System.out.println("4. Persegi Panjang");
                    System.out.println("0. Kembali ke menu sebelemunya");
                    System.out.println("----------------------------------");
                    System.out.print("Masukan Pilihan -> ");
                    pilihBidang = keyboard.nextLine();
                    switch (pilihBidang){
                        case "0":{
                            System.out.print("\nProcessing...\n");
                        }
                        break;

                        case "1":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih persegi");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan sisi : ");
                            double sisiPersegi = keyboard.nextDouble();

                            double luasPersegi = sisiPersegi * sisiPersegi;

                            System.out.print("\nProcessing...\n");
                            System.out.println("Luas dari persegi adalah " + luasPersegi);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        case "2":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih linkaran");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan jari jari : ");
                            double jariJariLingkaran = keyboard.nextDouble();

                            double luasLingkaran = phi * (jariJariLingkaran * jariJariLingkaran);

                            System.out.print("\nProcessing...\n");
                            System.out.println("Luas dari persegi adalah " + luasLingkaran);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        case "3":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih segitiga");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan alas : ");
                            double alasSegitiga = keyboard.nextDouble();
                            System.out.print("\nMasukan tinggi : ");
                            double tinggiSegitiga = keyboard.nextDouble();

                            double luasSegitiga = 0.5D * (alasSegitiga * tinggiSegitiga);

                            System.out.print("\nProcessing...\n");
                            System.out.println("Luas dari persegi adalah " + luasSegitiga);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        case "4":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih persegi panjang");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan panjang : ");
                            double panjangPersegiPanjang = keyboard.nextDouble();
                            System.out.print("\nMasukan lebar : ");
                            double lebarPersegiPanjang = keyboard.nextDouble();

                            double luasPersegipanjang = panjangPersegiPanjang * lebarPersegiPanjang;

                            System.out.print("\nProcessing...\n");
                            System.out.println("Luas dari persegi adalah " + luasPersegipanjang);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        default:{
                            System.out.print("Mohon maaf nomor yang anda pilih tidak ada atau tidak sesuai");
                            break;
                        }
                    }
                }
                break;

                case "2":{
                    System.out.println("----------------------------------");
                    System.out.println("Pilih bidang yang akan dihitung");
                    System.out.println("----------------------------------");
                    System.out.println("1. Kubus");
                    System.out.println("2. Balok");
                    System.out.println("3. Tabung");
                    System.out.println("0. Kembali ke menu sebelemunya");
                    System.out.println("----------------------------------");
                    System.out.print("Masukan Pilihan -> ");
                    pilihBidang = keyboard.nextLine();
                    switch (pilihBidang){
                        case "1":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih kubus");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan sisi : ");
                            double sisiKubus = keyboard.nextDouble();

                            double volumKubus = sisiKubus * sisiKubus * sisiKubus;

                            System.out.print("\nProcessing...\n");
                            System.out.println("Volum dari kubus adalah " + volumKubus);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        case "2":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih balok");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan panjang : ");
                            double panjangBalok = keyboard.nextDouble();
                            System.out.print("\nMasukan tinggi : ");
                            double tinggiBalok = keyboard.nextDouble();
                            System.out.print("\nMasukan lebar : ");
                            double lebarBalok = keyboard.nextDouble();

                            double volumBalok = panjangBalok * tinggiBalok * lebarBalok;

                            System.out.print("\nProcessing...\n");
                            System.out.println("Volum dari kubus adalah " + volumBalok);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        case "3":{
                            System.out.println("\n----------------------------------");
                            System.out.println("Anda memilih tabung");
                            System.out.println("----------------------------------");
                            System.out.print("Masukan jari jari : ");
                            double jariJariTabung = keyboard.nextDouble();
                            System.out.print("Masukan tinggi : ");
                            double tinggiTabung = keyboard.nextDouble();

                            double volumTabung = phi * (jariJariTabung * jariJariTabung) * tinggiTabung;

                            System.out.print("\nProcessing...\n");
                            System.out.println("Volum dari kubus adalah " + volumTabung);
                            System.out.println("----------------------------------");
                            System.out.print("tekan apa saja untuk kembali ke menu utama");
                            keyboard.nextLine();
                            BackToMenu();
                            break;
                        }

                        default:{
                            System.out.print("Mohon maaf nomor yang anda pilih tidak ada atau tidak sesuai");
                            break;
                        }
                    }
                }
                break;

                case "3":{
                    System.out.print("Ketik y untuk lanjutkan atau ketik apa saja untuk kembali ke menu utama -> ");
                    keluar = keyboard.nextLine();
                    if (!keluar.equals("y")) {
                        System.out.println("\nProcessing...");
                        break;
                    } else {
                        System.out.println("\nProcessing...");
                        System.exit(0);
                    }
                }
                break;

                default:{
                    System.out.print("Mohon maaf nomor yang anda pilih tidak ada atau tidak sesuai");
                    break;
                }
            }
        }while (true);
    }

    private static void BackToMenu() {
        Scanner keyboard = new Scanner(System.in);
        keyboard.nextLine();
        System.out.print("\nProcessing...\n");
    }
}
